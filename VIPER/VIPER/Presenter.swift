//
//  Presenter.swift
//  VIPER
//
//  Created by Darshan on 14/03/22.
//

import Foundation

//Object
//Protocol
//Reference View, Router, Interactor

protocol AnyPresenter{
    var router:AnyRouter? {get set}
    var interactor:AnyInteractor? {get set}
    var view:AnyView? {get set}
    
    func interactorDidFetchUser(with result: Result<[User],Error>)
    
}
class UserPresenter:AnyPresenter{
    var router: AnyRouter?
    
    var interactor: AnyInteractor?{
        didSet{
            interactor?.getUsers()
        }
    }
    
    var view: AnyView?
    
    func interactorDidFetchUser(with result: Result<[User], Error>) {
        switch result{
            case .success(let users):
                print(users)
                self.view?.updateWithUsers(with: users)
            case .failure(let error):
                print(error)
            self.view?.updateWithError(with: "\(error.localizedDescription)")
        }
    }
  
    
}
