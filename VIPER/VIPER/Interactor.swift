//
//  Interactor.swift
//  VIPER
//
//  Created by Darshan on 14/03/22.
//

import Foundation

//Object
//Protocol
//Reference Presenter

//
enum FetchError:Error{
    case failed
}
protocol AnyInteractor{
    var presenter:AnyPresenter?{get set}
    func getUsers()
}
class UserInteractor:AnyInteractor{
    
    
    var presenter:AnyPresenter?
    
    func getUsers() {
        guard let url = URL.init(string: "https://jsonplaceholder.typicode.com/users") else {return}
        let request = URLRequest.init(url: url)
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,error == nil else {
                self.presenter?.interactorDidFetchUser(with: .failure(FetchError.failed))
                return
            }
            do{
                let arrayUser = try JSONDecoder().decode([User].self, from: data)
                self.presenter?.interactorDidFetchUser(with: .success(arrayUser))
            }catch{
                self.presenter?.interactorDidFetchUser(with: .failure(FetchError.failed))

            }
        }.resume()
    }
}
