//
//  View.swift
//  VIPER
//
//  Created by Darshan on 14/03/22.
//

import Foundation
import UIKit
//View controller
//Presenter
//Reference Protocol

protocol AnyView{
    var presenter:AnyPresenter?{get set}
    func updateWithUsers(with users:[User])
    func updateWithError(with error:String)
}
class UserViewController:UIViewController,AnyView{
    var presenter:AnyPresenter?
    
    var refereshController:UIRefreshControl = UIRefreshControl()
    
    private let tableView:UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
       return table
    }()
    var arrayOfUser:[User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.tableView)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.refereshController.addTarget(self, action: #selector(refreshWatchListData(_:)), for: .valueChanged)
        self.tableView.addSubview(self.refereshController)
      
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.tableView.frame = self.view.frame
        self.tableView.backgroundColor = UIColor.white
    }
    func updateWithUsers(with users: [User]) {
        DispatchQueue.main.async {
            self.refereshController.endRefreshing()
            self.arrayOfUser = users
            self.tableView.reloadData()
        }
        
    }
    @objc private func refreshWatchListData(_ sender: Any) {
        self.presenter?.interactor?.getUsers()
    }
    func updateWithError(with error: String) {
        
    }
}
extension UserViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfUser.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(self.arrayOfUser[indexPath.row].name)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
