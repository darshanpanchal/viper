//
//  AppDelegate.swift
//  VIPER
//
//  Created by Darshan on 14/03/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let userRouter = UserRouter.start()
        guard let initialVC = userRouter.entry else { return true}
        let rootNC = UINavigationController(rootViewController: initialVC)
        window?.rootViewController = rootNC
        window?.makeKeyAndVisible()

        return true
    }

 


}

